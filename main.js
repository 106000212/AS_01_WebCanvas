////////////////////init var///////////////////////////////////
var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
canvas.width = window.innerWidth *0.4;
canvas.height = window.innerHeight*0.5;
context.fillStyle = "white";
canvas.style.cursor="pointer";
context.fillRect(0, 0, canvas.width, canvas.height);
var restore = new Array();
var resloc = -1;

var getColor = "black";
var getWidth = "3";
var brushmode="Brush";

var mousePressed = false;
var lastX, lastY;
///////////////////color size and x y//////////////////////////////////
function Colorpick(a){
    getColor =a.style.background;
};

function Brushsize(a){
    getWidth=a.innerHTML;
};
function getX(e){
    if(event.pageX==undefined){
        return event.targetTouches[0].pageX-canvas.offsetLeft;
    }else{
        return event.pageX-canvas.offsetLeft;
    }
}
function getY(e){
    if(event.pageY==undefined){
        return event.targetTouches[0].pageY-canvas.offsetTop;
    }else{
        return event.pageY-canvas.offsetTop;
    }
}
/////////////////////////////////////////////////////

/////////////////left column//////////////////////////////////

///////////////Text Input////////////////////////////////////////
function inputtext(e){
    document.getElementById("mycanvas").style.cursor="text";
    console.log("Text");
    context.fillStyle = getColor;
    var input = document.getElementById("text_input").value;
    var inputsize = document.getElementById("textsize_input").value;
    var inputfont = document.getElementById("textfont_input").value;
    console.log("Text Input:",input);
    console.log("Text size:",inputsize);
    textfont= inputsize+"px"+" "+inputfont;
    console.log("Text font:",textfont);
    context.font = textfont;
    context.fillText(input,lastX, lastY);
    cPush();
  };
////////////////////////////////////////////////////////////////////

//Imager Upload//////////////////////////////////////////////////
$('#image').click(function(){
    var src=document.getElementById("src_input").value
    var canvasPic = new Image();
    canvasPic.src = src;
    console.log("src=",src);
    canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
    console.log("Image Upload");
    cPush();
  });  
/////////////////////////////////////////////////////////////

/////////////////////Undo and Redo/////////////////////////////////////
function cPush() {
  resloc++;
  if (resloc < restore.length) {
       restore.length = resloc;
    }
  restore.push(canvas.toDataURL());
  console.log("cPush:",resloc);
}
function Undo(){
    if (resloc > 0) {
        resloc--;
        var canvasPic = new Image();
        canvasPic.src = restore[resloc];
        canvasPic.onload = function (){ context.drawImage(canvasPic, 0, 0); }
    }else{
        reset();
    }  
  console.log("undo:",resloc);
}

function Redo(){
    if (resloc < restore.length-1) {
        resloc++;
        var canvasPic = new Image();
        canvasPic.src = restore[resloc];
        canvasPic.onload = function (){ context.drawImage(canvasPic, 0, 0); }
    }
  console.log("redo",resloc);

}
//reset//////////////////////////////////////////////////////////////
function reset(){
    document.getElementById("rstbtn").addEventListener("click", function() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    cPush();
    context.rect(0, 0, canvas.width, canvas.height );
    context.fillStyle = "white";
    context.fill();
    
    restore = new Array();
    resloc =-1;
    }, false);
}
////////////////////////download/////////////////////////////////////////////

$("#save").click(function() {
    var html = " ";
    html += "<img src='" + canvas.toDataURL() + "' alt='from canvas'/>";
    var pageStyle = "<style>body{margin:0; padding: 0;}</style>";
    var tab = window.open();
    tab.document.write(html + pageStyle);
    console.log("Save by Web");
  });
//////////////////////////////////////////////////////////////////////

//////////////left column////////////////

//////////////////set brush type////////////////////////////
$("#eraser").click(function() { 
    canvas.style.cursor="pointer";
    brushmode="Eraser";
    console.log("Eraser");
  });

  $("#brush").click(function() {
    brushmode="Brush";
    canvas.style.cursor="crosshair";
    console.log("brush");
  });

  $("#circle").click(function() {
    brushmode="Circle";
    canvas.style.cursor="crosshair";
    console.log("circle");
  });

  $("#square").click(function() {
    brushmode="Square";
    canvas.style.cursor="help";
    console.log("square");
  });

  $("#triangle").click(function() {
    brushmode="Triangle";
    canvas.cursor="crosshair";
    console.log("triangle");
  });
/////////////////////////////////////////////////////////

/////////////////////addeventlisten//////////////////////////////////////
function Init() {
    $('#mycanvas').mousedown(function (e) {
        mousePressed = true;
        if(brushmode=="Brush"){
          Draw(getX(e),getY(e), false);
        }
        else if(brushmode=="Circle"){
          Draw_Circle(getX(e),getY(e), false);
        }
        else if(brushmode=="Square"){
          Draw_Rectangle(getX(e),getY(e), false);
        }
        else if(brushmode=="Triangle"){
          Draw_Triangle(getX(e),getY(e), false);
        }
        else{
          Draw(getX(e),getY(e), false);
        }
    });
    $('#mycanvas').mousemove(function (e) {
        if (mousePressed) {
          if(brushmode=="Brush"){
            Draw(getX(e),getY(e), true);
          }
          else if(brushmode=="Circle"){
            Draw_Circle(getX(e),getY(e), true);
          }
          else if(brushmode=="Square"){
            Draw_Rectangle(getX(e),getY(e), true); 
          }
          else if(brushmode=="Triangle"){
            Draw_Triangle(getX(e),getY(e), true);
          }
          else{
            Draw_eraser(getX(e),getY(e), true);
          }
         }
    });
    $('#mycanvas').mouseup(function (e) {
        mousePressed = false;
         cPush();
    });
    $('#mycanvas').mouseleave(function (e) {
        mousePressed = false;
    });
}
//////////////////////////////////////////////////////
////////////////////draw//////////////////////////////////
function Draw(x, y, isDown) {
    if (isDown) {
        context.beginPath();
        context.moveTo(lastX, lastY);
        context.lineTo(x, y);
        context.strokeStyle = getColor;
        context.lineWidth = getWidth;
        context.lineCap = "round";
        context.lineJoin = "round";
        context.closePath();
        context.stroke();
    }
    else
      cPush();
    lastX = x; lastY = y;
}

Init();

function Draw_Circle(x, y, isDown) {
    if (isDown) {
      context.fillStyle = getColor; 
      context.beginPath(); 
      context.arc(x,y,10+getWidth*1,0,Math.PI*2,true); 
      context.fill(); 
    }
    else
      cPush();
    lastX = x; lastY = y;
}

function Draw_Rectangle(x, y, isDown) {
    if (isDown) {
        context.beginPath();
        context.rect(x, y, 10+getWidth*1, 10+getWidth*1 );
        context.fillStyle = getColor;
        context.fill();
    }
    else
      cPush();
    lastX = x; lastY = y;
}
 
function Draw_Triangle(x, y, isDown) {
    if (isDown) {
        context.fillStyle = getColor; 
        context.beginPath();
        context.moveTo(x,y);
        context.lineTo(x-50-getWidth*1,y+25+getWidth*1);
        context.lineTo(x-50-getWidth*1,y-25);
        context.fill();
    }
    else
      cPush();
    lastX = x; lastY = y;
}

function Draw_eraser(x, y, isDown) {
    if (isDown) {
        context.beginPath();
        context.strokeStyle = "white";
        context.lineWidth = getWidth;
        context.lineCap = "round";
        context.lineJoin = "round";
        context.moveTo(lastX, lastY);
        context.lineTo(x, y);
        context.closePath();
        context.stroke();
    }
    else
      cPush();
    lastX = x; lastY = y;
}
/////////////////////////////////////////////////////////////////
