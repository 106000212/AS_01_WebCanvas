# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

## 功能講解
    這次將程式分成html,css,js三個檔案以下將分別講解功能
## index.html
* 大致上先將各元素分區塊弄好，將brush type and size的功能放在左半邊，canvas在中間，otherfunction放在右半邊，colorpicker則放在最下面
## main.css
* 將整個框架加以修飾，加入字型大小樣式，背景圖片等美化外觀的功能
## main.js
* brush type and size
    * brush<img src="brush.png" width="30px" height="30px"></img>
        * 最基本的筆刷，當按下brush圖片後，可在畫布上用刷子劃出圖案，這也是一開始打開頁面的預設功能，不一樣處只有指標圖案一開始預設是手指，預設顏色是黑色，筆刷大小是3，可利用colorpick改變顏色及brushsize改變大小，指標圖案是十字
    * eraser<img src="eraser.png" width="30px" height="30px"></img>
        * 當按下eraser圖片後，在畫布上的刷子就會變成橡皮擦，可將畫布上的東西擦掉，也可利用brushsize改變大小，指標圖案是手指
    * brushytpe<img src="circle.png" width="30px" height="30px"></img><img src="square.png" width="30px" height="30px"></img><img src="triangle.png" width="30px" height="30px"></img>
        * 此部分分成三個圖片，分別是圓形、正方形、三角形，按下對應的圖片，筆刷的形狀就會改變，同時其指標圖案也會有不同，圓形、三角形是十字、正方形則是問號，可利用colorpick改變顏色及brushsize改變大小
        * brushsize
            * 此部分是一個長條，可以將長條中的黑點往左或右移動，brush的大小也會跟著改變
* otherfunction
    * text
        * 此部分包含四個東西，分別依序是text大小、text字型、text內容等三個長條框及text按鈕，前三者輸入好後，先在畫布中想要的位置點一下，之後點選text按鈕，此時指標形狀會改變，並且會在指標按下的畫布位置出現設定好的文字，可利用colorpick改變顏色
    * upload image
        * 此部分包含一個load_image按鈕和一個長條框，在框框中輸入好要上傳圖片的位置(ex:C:/sky.jpg)後點選upload即可上傳
    * undo redo
        * undo<img src="undo.png" width="30px" height="30px"></img>：首先我設了變數負責計算已做過的動作數，並把每部做完動作的圖像從進矩陣，當按下undo圖片便會將resloc變數減1，顯示出矩陣裡resloc-1存的圖，便能完成回到上一步
        * redo<img src="redo.png" width="30px" height="30px"></img>：基本上方法一樣，不同的是判斷條件不同，在resloc<restore.length-1時會顯示resloc+1的圖，便能完成redo功能
    * reset
        * 按下reset按鈕後會將整個畫布清空，並將存undo redo的矩陣清空，故按此按鈕後無法再undo回沒reset的前一步
    * download
        * 按下此按鈕、現在的畫布畫面便會存成image.jpg，並在新網頁開啟所存畫布畫面，之後點選便可右鍵另存圖片
 * colorpicker
    * 此部分包含九個圈圈和一個格子，圈圈部分按下後就可按圈圈顯示的顏色在畫布上顯示出來
    * 若想選擇圈圈以外的顏色，點選最右邊旁的格子即可自由選擇深淺亮度彩度等等
    
   
    
   
